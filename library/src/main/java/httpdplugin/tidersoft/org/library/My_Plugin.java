package httpdplugin.tidersoft.org.library;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.Random;

import httpdplugin.tidersoft.org.library.com.example.unityplugin.Runer;
import httpdplugin.tidersoft.org.library.com.wargamer.server.Engine;

/**
 * Created by Tiderus on 16.03.2017.
 */

public class My_Plugin {




    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }


    public static String Download_System(final String system){
       // StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
      //  StrictMode.setThreadPolicy(policy);
        Thread th=new Thread(){
            public void run() {

                Engine.instance().Download_System(system);
            }};
        th.start();
        return "Download "+system+" Complite";
    }

    public static String Update(final String vers){
     //   StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
     //   StrictMode.setThreadPolicy(policy);
        Thread th=new Thread(){
            public void run() {

                Engine.instance().Download_Update(vers);
            }
        };
        th.start();
        return "Update Complite";
    }
    public static String Develop(){
       //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
      //  StrictMode.setThreadPolicy(policy);
        Thread th=new Thread(){
            public void run() {

                Engine.instance().Download_Develop();
            }
        };
        th.start();
        return "Update Complite";
    }

    public static String RandomID(){
        String text="";

        Random r=new Random();
        for(int i=0;i<32;i++){
            text += getchar(r.nextInt(60));

        }

        return text;


    }


    public static String getchar(int i){

        if(i==0)return "0";
        if(i==1)return "1";
        if(i==2)return "2";
        if(i==3)return "3";
        if(i==4)return "4";
        if(i==5)return "5";
        if(i==6)return "6";
        if(i==7)return "7";
        if(i==8)return "8";
        if(i==9)return "9";

        if(i==10)return "a";
        if(i==11)return "b";
        if(i==12)return "c";
        if(i==13)return "d";
        if(i==14)return "e";
        if(i==15)return "f";
        if(i==16)return "g";
        if(i==17)return "h";
        if(i==18)return "i";
        if(i==19)return "j";
        if(i==20)return "k";
        if(i==21)return "l";
        if(i==22)return "m";
        if(i==23)return "n";
        if(i==24)return "o";
        if(i==25)return "p";
        if(i==26)return "r";
        if(i==27)return "s";
        if(i==28)return "t";
        if(i==29)return "u";
        if(i==30)return "w";
        if(i==31)return "q";
        if(i==32)return "x";
        if(i==33)return "y";
        if(i==34)return "z";

        if(i==35)return "A";
        if(i==36)return "B";
        if(i==37)return "C";
        if(i==38)return "D";
        if(i==39)return "E";
        if(i==40)return "F";
        if(i==41)return "G";
        if(i==42)return "H";
        if(i==43)return "I";
        if(i==44)return "J";
        if(i==45)return "K";
        if(i==46)return "L";
        if(i==47)return "M";
        if(i==48)return "N";
        if(i==49)return "O";
        if(i==50)return "P";
        if(i==51)return "R";
        if(i==52)return "S";
        if(i==53)return "T";
        if(i==54)return "U";
        if(i==55)return "W";
        if(i==56)return "Q";
        if(i==57)return "X";
        if(i==58)return "Y";
        if(i==59)return "Z";




        return " ";
    }



    public static String StartServer(){

        Runer.Run();
        return "Server runed";
    }
}
