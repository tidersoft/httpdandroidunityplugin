package httpdplugin.tidersoft.org.library.com.wargamer.server.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

import httpdplugin.tidersoft.org.library.com.wargamer.server.Engine;


public class BroadcastClient {
    private DatagramSocket socket;
    private InetAddress address;
    private int expectedServerCount;
    private byte[] buf;

    public BroadcastClient(int expectedServerCount) throws Exception {
        this.expectedServerCount = expectedServerCount;
        this.address = InetAddress.getByName("255.255.255.255");
    }
    
    
    public static void pr(String text){
    System.out.println(text);
    }

    public int discoverServers(String msg) throws IOException {
        initializeSocketForBroadcasting();
        copyMessageOnBuffer(Engine.getLocalIpAddress());

        // When we want to broadcast not just to local network, call listAllBroadcastAddresses() and execute broadcastPacket for each value.
        broadcastPacket(address);

        return receivePackets();
    }



    private void initializeSocketForBroadcasting() throws SocketException {
        socket = new DatagramSocket();
        socket.setBroadcast(true);
    }

    private void copyMessageOnBuffer(String msg) {
        buf = msg.getBytes();
    }

    private void broadcastPacket(InetAddress address) throws IOException {
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 4445);
        socket.send(packet);
    }

    private int receivePackets() throws IOException {
        int serversDiscovered = 0;
        while (serversDiscovered != expectedServerCount) {
            receivePacket();
          //  serversDiscovered++;
        }
        return serversDiscovered;
    }

    private int receivePacket() throws IOException {
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        socket.receive(packet);
        ArrayList<String> ip = Engine.instance().IpAdress;
        String received = new String(packet.getData(), 0, packet.getLength());
        pr("Odpowiedział mi "+received);
        for(int i=0;i<ip.size();i++){
            if(ip.get(i).equals(received)){
                return 1;
            }
           
        }
         ip.add(received);
         pr("Dodaję "+received+" do listy serwerów");
         return 0;
    }

    public void close() {
        socket.close();
    }
}