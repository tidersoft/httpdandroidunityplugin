package httpdplugin.tidersoft.org.library.com.wargamer.server.util;


import java.util.LinkedList;

import httpdplugin.tidersoft.org.library.org.java_websocket.WebSocket;


/**
 *
 * @author Tiderus
 */

public class Sender_Browser implements Senders {
    public WebSocket conection;
	public String name;
	
	public LinkedList<String> sender;
	
	public Sender_Browser(){
		
		sender= new LinkedList<String>();
	}
         @Override
    public boolean isEmpty() {
   return sender.isEmpty();
    }

    @Override
    public String pop() {
     return sender.pop();
    }

    @Override
    public void send(String msg) {
   	conection.send(msg);
	
    }

    @Override
    public String name() {
      return name;
    }
}
