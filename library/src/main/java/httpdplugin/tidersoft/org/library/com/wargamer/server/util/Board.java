package httpdplugin.tidersoft.org.library.com.wargamer.server.util;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Tiderus on 25.01.2018.
 */

public class Board {

    public String System;
    public String name;
    public HashMap<String,Token> tokeny;
    public ArrayList<Sender_Game> conection;
    public HashMap<String,Var> vars;

    public Board(){
        vars= new HashMap<String,Var>();
        tokeny = new HashMap<>();
        conection = new ArrayList<>();
    }
}
