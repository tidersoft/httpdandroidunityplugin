package httpdplugin.tidersoft.org.library.com.wargamer.server.util;

import java.util.LinkedList;

import httpdplugin.tidersoft.org.library.org.java_websocket.WebSocket;


public class Sender_Game implements Senders {

	public WebSocket conection;
	public String name;
	
	public LinkedList<String> sender;
	
	public Sender_Game(){
		
		sender= new LinkedList<String>();
	}

    @Override
    public boolean isEmpty() {
   return sender.isEmpty();
    }

    @Override
    public String pop() {
     return sender.pop();
    }

    @Override
    public void send(String msg) {
        System.out.println("Sending "+conection + " : "+msg);
  	conection.send(msg);
	
    }

    @Override
    public String name() {
   return name;
    }
	
	
}
