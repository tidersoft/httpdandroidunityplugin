/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package httpdplugin.tidersoft.org.library.com.wargamer.server.util;

/**
 *
 * @author Tiderus
 */
public interface Senders {
    
    public boolean isEmpty();
    public String pop();
    public void send(String msg);
    public String name();
}
