package httpdplugin.tidersoft.org.library.com.wargamer.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.logging.Logger;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;
import httpdplugin.tidersoft.org.library.com.wargamer.server.util.Player;
import httpdplugin.tidersoft.org.library.org.nanohttpd.InternalRewrite;
import httpdplugin.tidersoft.org.library.org.nanohttpd.WSServer;
import httpdplugin.tidersoft.org.library.org.nanohttpd.WebServerPlugin;

/*
 * #%L
 * NanoHttpd-Websocket
 * %%
 * Copyright (C) 2012 - 2015 nanohttpd
 * %%
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the nanohttpd nor the names of its contributors
 *    may be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * #L%
 */


/**
 * @author Paul S. Hawke (paul.hawke@gmail.com) On: 4/23/14 at 10:31 PM
 */
public class WargamerServer extends NanoHTTPD {

    /**
     * logger to log to.
     */
    private static final Logger LOG = Logger.getLogger(WargamerServer.class.getName());

    private final boolean debug;


    public HashMap<String,HTTPSession> sesje;

    public WSServer wsServer;
    
    
    public WargamerServer(int port, boolean debug) {
        super(port);
        this.debug = debug;
        sesje = new HashMap<String,HTTPSession>();

        System.out.println("HTTPD Server start at port: "+port);
    }

   
    
    
    public String RandomID(){
    	String text="";
    	
    	Random r=new Random();
    	for(int i=0;i<32;i++){
    		text += getchar(r.nextInt(60));
    		
    	}
    	
    	return text;
    	
    	
    }
    
    
  public String getchar(int i){
    	
    	if(i==0)return "0";
    	if(i==1)return "1";
    	if(i==2)return "2";
    	if(i==3)return "3";
    	if(i==4)return "4";
    	if(i==5)return "5";
    	if(i==6)return "6";
    	if(i==7)return "7";
    	if(i==8)return "8";
    	if(i==9)return "9";
    	
    	if(i==10)return "a";
    	if(i==11)return "b";
    	if(i==12)return "c";
    	if(i==13)return "d";
    	if(i==14)return "e";
    	if(i==15)return "f";
    	if(i==16)return "g";
    	if(i==17)return "h";
    	if(i==18)return "i";
    	if(i==19)return "j";
    	if(i==20)return "k";
    	if(i==21)return "l";
    	if(i==22)return "m";
    	if(i==23)return "n";
    	if(i==24)return "o";
    	if(i==25)return "p";
    	if(i==26)return "r";
    	if(i==27)return "s";
    	if(i==28)return "t";
    	if(i==29)return "u";
    	if(i==30)return "w";
    	if(i==31)return "q";
    	if(i==32)return "x";
    	if(i==33)return "y";
    	if(i==34)return "z";
    	
    	if(i==35)return "A";
    	if(i==36)return "B";
    	if(i==37)return "C";
    	if(i==38)return "D";
    	if(i==39)return "E";
    	if(i==40)return "F";
    	if(i==41)return "G";
    	if(i==42)return "H";
    	if(i==43)return "I";
    	if(i==44)return "J";
    	if(i==45)return "K";
    	if(i==46)return "L";
    	if(i==47)return "M";
    	if(i==48)return "N";
    	if(i==49)return "O";
    	if(i==50)return "P";
    	if(i==51)return "R";
    	if(i==52)return "S";
    	if(i==53)return "T";
    	if(i==54)return "U";
    	if(i==55)return "W";
    	if(i==56)return "Q";
    	if(i==57)return "X";
    	if(i==58)return "Y";
    	if(i==59)return "Z";
    	
    	
    	
    	
    	return " ";
    }
    

  public boolean setSession(String sessionID, String login,String icon,String lang,HTTPSession ses){


  	
  	if(Engine.instance().players.containsKey(login) || sesje.containsKey(sessionID))
        return false;

      sesje.put(sessionID,ses);


      Player pl=new Player(login);
        pl.icona=icon;
        pl.lang=lang;
  		//pl.tab=engine.stoly.get(0);
  		Engine.instance().players.put(login,pl );
        Engine.Engine_Mesg("AddPlayer","{\"Name\":\""+login+"\",\"Icon\":\""+icon+"\"}");


      Engine.instance().RefreshPlayer();
     	  
  	return true;
  }
  
    @Override
    public Response serve(IHTTPSession session) {
        Method method = session.getMethod();
        String uri = session.getUri();
        Map<String, String> parms = session.getParms();
        Map<String, String> post_params = new HashMap<>();
        if(method.equals(Method.POST)){
        try {
			session.parseBody(post_params);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ResponseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}}
        
        System.out.println( session.getMethod() + " " + session.getParms() );
        System.out.println(method + " '" + uri + "' ");
      //  LOG.info(method + " '" + uri + "' ");

        String msg = "";

        String login=session.getCookies().read("Wargamer");
        
        if(login==null || login=="")
            login=this.RandomID();
    
        
        
       
        


        
      
        if(uri.contentEquals("/logout")){
      	  session.getCookies().delete("Wargamer");
      	
              Engine.instance().Logout(login);
              
              
      	   sesje.remove(login);
      	   uri="/";
            login=this.RandomID();

            //   engine.CreatePlayer();
         }
        
      
        
        if(method.equals(Method.POST)){
        	  if(post_params.containsKey("login") && post_params.containsKey("avatar") && post_params.containsKey("lang")){

                  if(setSession(post_params.get("login"),post_params.get("login"),post_params.get("avatar"),post_params.get("lang"),(HTTPSession)session)) {
                      session.getCookies().set("Wargamer", post_params.get("login"), 1);
                      login = post_params.get("login");

                  }
        	   }
        	  
        	
        	   if(post_params.containsKey("action")){

        	      if(post_params.get("action").equalsIgnoreCase("savefile") && post_params.containsKey("path")&& post_params.containsKey("edit")){
                        File file  = new File(Engine.getPath()+post_params.get("path"));
                      try {

                          if(!file.exists()){
                                file.createNewFile();


                        }
                        FileOutputStream io = new FileOutputStream(file);
                          io.write(post_params.get("edit").getBytes());
                          io.flush();
                          io.close();
                      } catch (IOException e) {
                          e.printStackTrace();
                      }

                  }

               }
        	
        }


        if(uri.equals("/PostMessage")){
          Engine.instance().PostMessage(parms);
        }

        if(uri.equals("/PutOnServer")){
            if(parms.containsKey("database") && parms.containsKey("name") && parms.containsKey("value"))
            Engine.instance().Put_On_Server(parms.get("database"),parms.get("name"),parms.get("value"));

        }

        if(uri.equals("/UpdateOnServer")){
            if(parms.containsKey("database") && parms.containsKey("id") && parms.containsKey("value"))

                Engine.instance().Update_On_Server(parms.get("database"),Integer.parseInt(parms.get("id")),parms.get("value"));

        }
        if(uri.equals("/DeleteOnServer")){
            if(parms.containsKey("database") && parms.containsKey("id") )

                Engine.instance().Delete_On_Server(parms.get("database"),Integer.parseInt(parms.get("id")));

        }

        if(uri.equals("/GetValueById")){
            if(parms.containsKey("database") && parms.containsKey("id") )

                msg+=Engine.instance().Get_Value_By_Id(parms.get("database"),Integer.parseInt(parms.get("id")));
        }

        if(uri.equals("/GetValueByName")){
            if(parms.containsKey("database") && parms.containsKey("name") )

                msg+=Engine.instance().Get_Value_By_Name(parms.get("database"),parms.get("name"));
        }



        if(uri.equals("/GetDataBase")){
            if(parms.containsKey("database"))

                msg+=Engine.instance().Get_DataBase(parms.get("database"));
        }

        if(uri.equals("/SendToAll")){
            if(parms.containsKey("message")) {
                wsServer.sendToAll(parms.get("message"));
            }
        }

        if(uri.equals("/GetBoards")){
            if(parms.containsKey("system")){
                msg+=Engine.instance().getBoards(parms.get("system"));
            }

        }

     //   if(uri.equals("/GetModule")){
      //  	if(parms.containsKey("system")&&parms.containsKey("modul")){
      //  		uri="/dane/Systemy/"+parms.get("system")+"/Module/"+parms.get("modul");
      // /
      //  	}



      //  }


        if(uri.equals("/GetSystems")){

            msg+=Engine.instance().getSystems();
        }

        if(uri.equals("/CreateGame")){
            if(parms.containsKey("name")){
                Engine.instance().Create_Game(parms.get("name"),login,parms.get("system"));

            }

        }

        if(uri.equals("/GetGame")){
            msg+=Engine.instance().getGame(login);

        }
        
        if(uri.equals("/GetFiles")){

            if(parms.containsKey("path"))
        	msg+=Engine.instance().getFiles(parms.get("path"));
        	
        }

        if(uri.equals("/GetMe")){
            if(Engine.instance().players.containsKey(login)){
                Player pl=Engine.instance().players.get(login);
                msg+="{\"Name\":\""+pl.name+"\",\"Icon\":\""+pl.icona+"\",\"Lang\":\""+pl.lang+"\"}";
            }

        }

        if(uri.equals("/GetPlayers")){
            msg+= Engine.instance().getPlayers();

        }
        if(uri.equals("/GetOuters")){
           msg+= Engine.instance().getOuterPlayers(login);

        }
        
        System.out.println("Login: "+login);
        
        if(uri.equals("/")){
        	if(sesje.containsKey(login)){
        		uri="/index.html";
        	}else{
        		uri="/register.html";
        		
        	}
        	
        	
        	
      	   
        }
        
        
        
        
  File te= new File(Engine.getPath()+uri);
        
        
        if(te.isFile()){
       	 String mimeTypeForFile = getMimeTypeForFile(uri);
            
       	 Response resp= serveFile(uri,session.getHeaders(),te,mimeTypeForFile,login);
       	 
       	   resp.addHeader("Access-Control-Allow-Origin", "*");
       	    	 
       return resp;
       }
        
        
        Response resp=newFixedLengthResponse(msg);
        SimpleDateFormat gmtFrmt = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss 'GMT'", Locale.US);
        gmtFrmt.setTimeZone(TimeZone.getTimeZone("GMT"));

        	resp.addHeader("Last-modified", gmtFrmt.format(new Date()));
        	resp.addHeader("Expires",gmtFrmt.format(new Date()) );
            
        	String ip = "*";
		///	try {
		//		ip = "http://"+Inet4Address.getLocalHost().getHostAddress()+":8080";
			
        	
        	resp.addHeader("Access-Control-Allow-Origin",ip );
		//	} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
		//		e.printStackTrace();
		//	}
           return resp;
      
    }



    public static String read(File fis)throws IOException{

        BufferedReader dane;
        StringBuffer sb = new StringBuffer();



        dane=new BufferedReader(new FileReader(fis));
        String s;

        while((s = dane.readLine())!=null){
            sb.append(s);
            sb.append("\n");
        }
        dane.close();
        return sb.toString();





    }
    
 public static  String MIME_DEFAULT_BINARY = "application/octet-stream";

    
    /**
     * Default Index file names.
     */
    @SuppressWarnings("serial")
    public static  List<String> INDEX_FILE_NAMES = new ArrayList<String>() {

        {
            add("index.html");
            add("index.htm");
        }
    };

    @SuppressWarnings("serial")
    private static Map<String, String> MIME_TYPES = new HashMap<String, String>() {

        {
            put("css", "text/css");
            put("htm", "text/html");
            put("html", "text/html");
            put("xml", "text/xml");
            put("java", "text/x-java-source, text/java");
            put("md", "text/plain");
            put("txt", "text/plain");
            put("asc", "text/plain");
            put("gif", "image/gif");
            put("jpg", "image/jpeg");
            put("jpeg", "image/jpeg");
            put("png", "image/png");
            put("mp3", "audio/mpeg");
            put("m3u", "audio/mpeg-url");
            put("mp4", "video/mp4");
            put("ogv", "video/ogg");
            put("flv", "video/x-flv");
            put("mov", "video/quicktime");
            put("swf", "application/x-shockwave-flash");
            put("js", "application/javascript");
            put("pdf", "application/pdf");
            put("doc", "application/msword");
            put("ogg", "application/x-ogg");
            put("zip", "application/octet-stream");
            put("exe", "application/octet-stream");
            put("class", "application/octet-stream");
        }
    };
 
    
    private boolean canServeUri(String uri, File homeDir) {
        boolean canServeUri;
        File f = new File(homeDir, uri);
        canServeUri = f.exists();
        if (!canServeUri) {
            String mimeTypeForFile = getMimeTypeForFile(uri);
            WebServerPlugin plugin = mimeTypeHandlers.get(mimeTypeForFile);
            if (plugin != null) {
                canServeUri = plugin.canServeUri(uri, homeDir);
            }
        }
        return canServeUri;
    }
    
    private static Map<String, WebServerPlugin> mimeTypeHandlers = new HashMap<String, WebServerPlugin>();


    /**
     * URL-encodes everything between "/"-characters. Encodes spaces as '%20'
     * instead of '+'.
     */
    private String encodeUri(String uri) {
        String newUri = "";
        StringTokenizer st = new StringTokenizer(uri, "/ ", true);
        while (st.hasMoreTokens()) {
            String tok = st.nextToken();
            if (tok.equals("/")) {
                newUri += "/";
            } else if (tok.equals(" ")) {
                newUri += "%20";
            } else {
                try {
                    newUri += URLEncoder.encode(tok, "UTF-8");
                } catch (UnsupportedEncodingException ignored) {
                }
            }
        }
        return newUri;
    }

    private String findIndexFileInDirectory(File directory) {
        for (String fileName : INDEX_FILE_NAMES) {
            File indexFile = new File(directory, fileName);
            if (indexFile.isFile()) {
                return fileName;
            }
        }
        return null;
    }

    protected Response getForbiddenResponse(String s) {
        return newFixedLengthResponse(Status.FORBIDDEN, NanoHTTPD.MIME_PLAINTEXT, "FORBIDDEN: " + s);
    }

    protected Response getInternalErrorResponse(String s) {
        return newFixedLengthResponse(Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, "INTERNAL ERROR: " + s);
    }

    // Get MIME type from file name extension, if possible
    public static String getMimeTypeForFile(String uri) {
        int dot = uri.lastIndexOf('.');
        String mime = null;
        if (dot >= 0) {
            mime = MIME_TYPES.get(uri.substring(dot + 1).toLowerCase());
        }
        return mime == null ? MIME_DEFAULT_BINARY : mime;
    }

    protected Response getNotFoundResponse() {
        return newFixedLengthResponse(Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, "Error 404, file not found.");
    }
    
    
    protected static void registerPluginForMimeType(String[] indexFiles, String mimeType, WebServerPlugin plugin, Map<String, String> commandLineOptions) {
        if (mimeType == null || plugin == null) {
            return;
        }

        if (indexFiles != null) {
            for (String filename : indexFiles) {
                int dot = filename.lastIndexOf('.');
                if (dot >= 0) {
                    String extension = filename.substring(dot + 1).toLowerCase();
                    MIME_TYPES.put(extension, mimeType);
                }
            }
            INDEX_FILE_NAMES.addAll(Arrays.asList(indexFiles));        }
       mimeTypeHandlers.put(mimeType, plugin);
        plugin.initialize(commandLineOptions);
    }

    private  boolean quiet;

    protected List<File> rootDirs;


    
    
    private Response respond(Map<String, String> headers, IHTTPSession session, String uri,String login) {
        // Remove URL arguments
        uri = uri.trim().replace(File.separatorChar, '/');
        if (uri.indexOf('?') >= 0) {
            uri = uri.substring(0, uri.indexOf('?'));
        }

        // Prohibit getting out of current directory
        if (uri.contains("../")) {
            return getForbiddenResponse("Won't serve ../ for security reasons.");
        }

        boolean canServeUri = false;
        File homeDir = null;
        for (int i = 0; !canServeUri && i < this.rootDirs.size(); i++) {
            homeDir = this.rootDirs.get(i);
            canServeUri = canServeUri(uri, homeDir);
        }
        if (!canServeUri) {
            return getNotFoundResponse();
        }

        // Browsers get confused without '/' after the directory, send a
        // redirect.
        File f = new File(homeDir, uri);
      

      

        String mimeTypeForFile = getMimeTypeForFile(uri);
        WebServerPlugin plugin = mimeTypeHandlers.get(mimeTypeForFile);
        Response response = null;
        if (plugin != null && plugin.canServeUri(uri, homeDir)) {
            response = plugin.serveFile(uri, headers, session, f, mimeTypeForFile);
            if (response != null && response instanceof InternalRewrite) {
                InternalRewrite rewrite = (InternalRewrite) response;
                return respond(rewrite.getHeaders(), session, rewrite.getUri(),login);
            }
        } else { 
            response = serveFile(uri, headers, f, mimeTypeForFile,login);
        }
        return response != null ? response : getNotFoundResponse();
    }

 

    /**
     * Serves file from homeDir and its' subdirectories (only). Uses only URI,
     * ignores all headers and HTTP parameters.
     */
    Response serveFile(String uri, Map<String, String> header, File file, String mime,String login) {
        Response res;
        try{

            // Change return code and add Content-Range header when skipping is
            // requested
            long fileLen = file.length();




            // supply the file
            res = newFixedFileResponse(file, mime,login);
            res.addHeader("Content-Length", "" + fileLen);


        } catch (IOException ioe) {
            res = getForbiddenResponse("Reading file failed.");
        }

        return res;
    }




    private Response newFixedFileResponse(File file, String mime,String login) throws FileNotFoundException {
        Response res;
        if(mime.equalsIgnoreCase(MIME_HTML)) {
            String text = null;
            try {
                text = read(file);
               // Iterator<Board> it=Engine.instance().boards.values().iterator();

            //    while(it.hasNext()){
              //      Board var = it.next();
              //      String old = "{:"+var.name+":}";
             //       String nowy = var.value;
             //       text=text.replace(old,nowy);
             //   }



                if(Engine.instance().players.containsKey(login)) {
                    Player player = Engine.instance().players.get(login);
                    String old = "{:System:Icon:Me:}";
                    String nowy = player.icona;
                    text = text.replace(old, nowy);
                    old = "{:System:Lang:Me:}";
                    nowy = player.lang;
                    text = text.replace(old, nowy);
                    old = "{:System:Name:Me:}";
                    nowy = player.name;
                    text = text.replace(old, nowy);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            res = newFixedLengthResponse(Status.OK, mime,text );

        }else{
            res = newFixedLengthResponse(Status.OK, mime,new FileInputStream(file),file.length() );

        }
        // InputStream is = new ByteArrayInputStream(getFileContent(file).getBytes());

        res.addHeader("Accept-Ranges", "bytes");
        return res;
    }

	
  
}
