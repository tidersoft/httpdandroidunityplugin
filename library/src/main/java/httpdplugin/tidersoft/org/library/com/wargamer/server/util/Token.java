package httpdplugin.tidersoft.org.library.com.wargamer.server.util;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Tiderus on 25.01.2018.
 */

public class Token {

    public String name;
    public float x,y,z;
    public ArrayList<Counter> conters;
    public HashMap<String ,Var> vars;

    public Token(){
        conters= new ArrayList<Counter>();
        vars= new HashMap<String,Var>();
    }
}
